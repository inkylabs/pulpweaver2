import { visit } from 'unist-util-visit'

export default (config) => {
  return (opt) => {
    return (root, f) => {
      visit(root, 'textDirective', (node, index, parent) => {
        if (node.name === 'booktitle') {
          parent.children.splice(index, 1, {
            type: 'text',
            position: node.position,
            value: config.title
          })
        }
        if (node.name === 'booksubtitle') {
          parent.children.splice(index, 1, {
            type: 'text',
            position: node.position,
            value: config.subtitle
          })
        }
      })
    }
  }
}
