import Cite from 'citation-js'
import { promises as fs } from 'fs'
import { globby } from 'globby'
import path from 'path'
import { remark } from 'remark'
import control from 'remark-message-control'
import resolveCwd from 'resolve-cwd'
import { toVFile } from 'to-vfile'
import { reporter } from 'vfile-reporter'

import { red } from './util.js'
import builtinPlugin from './plugin.js'

async function fileIfExists (path) {
  let s
  try {
    s = await fs.stat(path)
  } catch (e) {
    if (e.code === 'ENOENT') return undefined
    throw e
  }
  return s.isFile() ? path : undefined
}

async function expandFiles (flist) {
  if (typeof flist === 'string') flist = [flist]
  const ret = []
  for (const f of flist) {
    try {
      ret.push(...await globby(f))
    } catch (e) {
      if (e.code === 'EACCES') {
        console.error(`could not expand "${f}"`)
        continue
      }
    }
  }
  return ret
}

async function resolveMod (mod) {
  if (typeof mod !== 'string') return mod
  let loc
  try {
    loc = resolveCwd(mod)
  } catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') throw e
    console.error(e)
    return null
  }

  try {
    const { default: d } = await import(loc)
    return d
  } catch (e) {
    if (e instanceof SyntaxError) {
      console.error(`${loc}: ${e.message}`)
    } else {
      console.error(e)
    }
  }
  return null
}

async function getRemark (config, outConfig) {
  const plugins = [
    builtinPlugin(config),
    ...config.remark.plugins,
    ...outConfig.remark.plugins
  ]
  let chain = remark()
  for (const spec of plugins) {
    let mod = spec
    let opts
    if (Array.isArray(spec)) {
      mod = spec[0]
      opts = spec[1]
    }

    mod = await resolveMod(mod)
    if (mod) chain = chain.use(mod, opts)
  }
  chain.use(control, { name: 'lint', source: config.remark.sources })
  return chain
}

async function loadSources (config) {
  const files = await expandFiles(config.bibfiles)
  const sources = {}
  for (const f of files) {
    const content = await fs.readFile(f, 'utf8')
    const cleanContent = content.replace(/^ *%.*/gm, '')
    for (const source of Cite.input(cleanContent)) {
      if (source.id in sources) {
        console.error(`duplicate source: ${source.id}`)
      }
      sources[source.id] = source
    }
  }
  return sources
}

async function buildForOutput (outConfig, config) {
  config = Object.assign({
    buildDir: 'build',
    chapters: ['chapters/*.md'],
    appendices: ['appendices/*.md'],
    bibfiles: ['sources*.bib'],
    description: await fileIfExists('description.md'),
    preamble: await fileIfExists('preamble.md'),
    endorsements: await fileIfExists('endorsements.md'),
    halftitlepage: await fileIfExists('halftitlepage.md'),
    titlepage: await fileIfExists('titlepage.md'),
    copyright: await fileIfExists('copyright.md'),
    dedication: await fileIfExists('dedication.md'),
    errata: await fileIfExists('errata.md'),
    foreword: await fileIfExists('foreword.md'),
    bibliography: false,
    tableofcontents: false,
    listoffigures: false,
    indices: [],
    date: new Date(),
    remark: {}
  }, config)
  config.remark = Object.assign({
    sources: [],
    plugins: []
  }, config.remark)
  config.chapters = await expandFiles(config.chapters)
  config.appendices = await expandFiles(config.appendices)
  config.bibfiles = await expandFiles(config.bibfiles)
  config.sources = await loadSources(config)
  outConfig = Object.assign({
    buildDir: outConfig.name,
    remark: {
      plugins: []
    }
  }, outConfig)
  outConfig.remark = Object.assign({
    plugins: []
  }, outConfig.remark)

  if (!outConfig.name) {
    console.error(`Output needs a name: ${JSON.stringify(outConfig, null, 2)}`)
    return
  }
  if (!outConfig.plugin) {
    console.error(`Output "${outConfig.name}" needs a plugin`)
    return
  }

  const outmod = await resolveMod(outConfig.plugin)
  if (!outmod) {
    console.error(`Cannot find module ${outConfig.plugin}`)
    return
  }
  const resolvedOutConfig = await outmod(outConfig.opts)

  const buildDir = path.join(config.buildDir, outConfig.buildDir)
  try {
    await fs.mkdir(buildDir, { recursive: true })
    await fs.symlink(
      path.join('..', '..', 'figs'), path.join(buildDir, 'figs'))
  } catch (e) {
    if (e.code !== 'EEXIST') throw e
  }

  const cs = [
    {
      path: config.preamble,
      type: 'preamble'
    },
    {
      path: outConfig.preamble,
      type: 'preamble'
    },
    {
      path: config.description,
      type: 'description'
    },
    {
      path: config.errata,
      type: 'errata'
    },
    {
      path: config.titlepage,
      type: 'titlepage'
    },
    {
      path: config.halftitlepage,
      type: 'halftitlepage'
    },
    {
      path: config.endorsements,
      type: 'endorsements'
    },
    {
      path: config.copyright,
      type: 'copyright'
    },
    {
      path: config.dedication,
      type: 'dedication'
    },
    {
      path: config.foreword,
      type: 'foreword'
    },
    ...config.chapters.map(c => ({
      path: c,
      type: 'chapter'
    })),
    ...config.appendices.map(c => ({
      path: c,
      type: 'appendix'
    }))
  ]
    .filter(c => !!c.path)
  for (let i = 0; i < cs.length; i++) {
    const fp = cs[i].path
    const type = cs[i].type
    const f = await toVFile.read(fp)
    const chain = await getRemark(config, outConfig)
    const res = await chain
      .use(resolvedOutConfig.remarkFilePlugin, {
        chapterindex: i,
        type,
        sources: config.sources
      })
      .process(f)
    res.messages = res.messages
      .filter(m => m.ruleId !== 'no-file-name-articles')
      .filter(m =>
        m.ruleId !== 'no-emphasis-as-heading' ||
        m.file !== config.copyright)
    if (res.messages.length) {
      console.error(reporter(res, {
        quiet: true
      }))
    }
    const ext = resolvedOutConfig.fileExt
    const outPath = path.join(buildDir, 'genfiles', `${f.path}.${ext}`)
    try {
      await fs.mkdir(path.dirname(outPath), { recursive: true })
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.writeFile(outPath, res.value)
  }

  const cwd = process.cwd()
  process.chdir(buildDir)
  const success = await resolvedOutConfig.buildAfterFiles(config)
  process.chdir(cwd)
  return success
}

async function build (config, name = '.*') {
  const nameRe = new RegExp(name)
  const cwd = process.cwd()

  const outputs = config.output
    .filter(o => o.name.match(nameRe))
  if (!outputs.length) {
    red(`output spec "${name}" does not match any in the configuration.`)
  }
  let success = true
  for (const o of outputs) {
    success = success && await buildForOutput(o, config)
  }

  process.chdir(cwd)
  if (!success) red('build failed')
  return success
}

export default build
