import chalk from 'chalk'

const red = m => console.error(chalk.red(m))

export {
  red
}
