#!/usr/bin/env node
import optimist from 'optimist'
import path from 'path'

import build from '../lib/build.js'
import { red } from '../lib/util.js'

const SUBCMDS = {
  build: {
    fn: buildMain,
    opt: optimist.usage('Usage: $0 build <dir?>'),
    desc: 'Build the project'
  }
}

function oc (f, msg) {
  f.toString = () => msg
  return f
}

async function buildMain (config, { _: [name] }) {
  name = name || '.*'
  const res = await build(config, name)
  return res ? 0 : 1
}

async function loadConfig () {
  const configPath = path.resolve('pulpweaver.config.js')
  let mod
  try {
    mod = await import(configPath)
  } catch (e) {
    if (e.code === 'ERR_MODULE_NOT_FOUND') {
      red(e.message)
      return null
    }
    if (e instanceof SyntaxError) {
      red(`Could not load "${configPath}"; bad syntax.`)
      return null
    }
    throw e
  }

  const config = mod.default
  if (!config) {
    red(`No default export found in ${configPath}.`)
    return null
  }

  if (!config.output || !config.output.length) {
    red('No output specified in pulpweaver config')
    return null
  }

  const outputNames = {}
  for (const o of config.output) {
    if (o.name in outputNames) {
      red(`Duplicate output name: ${o.name}`)
      continue
    }
    outputNames[o.name] = true
  }

  return config
}

async function main () {
  const help = Object.keys(SUBCMDS)
    .sort()
    .map(k => `${k} ${SUBCMDS[k].desc}`)
    .join('\n')
  const o = optimist.usage(`Usage: $0 <subcommand>\n${help}`)
    .check(oc(({ _ }) => !!_.length, 'must provide a subcommand'))
    .check(oc(({ _: [s] }) => !!SUBCMDS[s], 'subcommand not recognized'))
  const s = SUBCMDS[o.argv._[0]]
  const argv = s.opt.argv
  argv._.shift() // Remove the subcommand

  const config = await loadConfig()
  if (!config) return 1

  return await s.fn(config, argv)
}

(async () => {
  let ret = -1
  try {
    ret = await main()
  } catch (e) {
    console.error(e)
    console.error(e.stack)
  }
  process.exit(ret) // eslint-disable-line n/no-process-exit
})()
